// Package hexa is a framework for interceptors without a network layer
//
// The framework is compatible with service oriented architectures such as
// Hexagonal, Clean, Onion, and Ports and Adapters.  It allows middleware to
// interact with the request and response of direct method calls without
// losing type safety and direct method calls.
//
// The only requirement to use this framework is to separate the service definition
// from the implementation to allow the invocation of the interceptor chain.
//
//	package my_service
//
//	// Bind chain to implementation
//	func NewMyService() (*MyService) {
//		return &MyService{
//			h: hexa.NewChain("service_chain", hexa.Interceptors([]hexa.Interceptor{MyInterceptor})),
//			i: &MyServiceImpl{},
//		}
//	}
//	// Service definition the caller imports and uses
//	func (s *MyService) ServiceMethod(ctx context.Context, arg string) (bool, error) {
//		// Invoke the interceptor chain
//		return hexa.Handle1(s.h, 0, s.i.ServiceMethodImpl, ctx, arg)
//	}
//
// The caller imports the service definition and calls the service directly
// without any knowledge of the interceptor chain.
//
//	package service_client
//
//	import "my_service"
//
//	func main() {
//		// Call the service directly with no knowledge of the interceptor chain
//		myService := NewMyService()
//		res, err := myService.ServiceMethod(context.Background(), "test")
//		fmt.Printf("Result: %v, Error: %v\n", res, err)
//	}
//
// You may use the Handle0, Handle1, Handle2 to handle service methods with a
// context and 0, 1, or 2 arguments, respectively.  These methods expect the
// service method to return a result and an error.  If the service method does not
// return a response and only returns an error, you may use the HandleNoResult0,
// HandleNoResult1, and HandleNoResult2 methods.
//
// If you require more flexibility, i.e. more than 2 arguments, and or a
// different mix of result types in your service method then you may use the
// HandleN method.  Note that there is no type checking done on the HandleN
// method so typically you should use the recommended code generation tool
// to generate the service definition layer.  This code generation will make
// use of HandleN and ensure that the arguments and result types match the
// service method.
package hexa

import (
	"context"
	"fmt"
	"reflect"
	"runtime"
	"strings"

	"github.com/samber/lo"
)

var (
	ErrInvalidResponse = fmt.Errorf("invalid response")
)

// EmptyResult is used for implementation functions that don't return any result.
type EmptyResult struct{}

// EmptyArg is used for implementation functions that don't take any arguments.
type EmptyArg struct{}

// Tag is a struct that holds metadata about a request.
type Tag struct {
	Key   string
	Value string
}

// T is a convenience function to create a Tag.
func T(key string, value string) Tag {
	return Tag{Key: key, Value: value}
}

// ResponseWriter is a struct that holds the reflected values returned from
// the call to the service implementation.  These values can be inspected
// and potentially modified by the interceptor chain in the defer paths.
type ResponseWriter struct {
	values []reflect.Value
}

// SetValues is simple setter for the values field.
func (r *ResponseWriter) SetValues(values ...reflect.Value) {
	if r != nil {
		r.values = values
	}
}

// Response is a convenience method used to parse the reflected values returned
// by the service implementation.  It attempts to return the response as an
// `any, error` by inspected the reflected values and type converting them.
func (r *ResponseWriter) Response() (any, error) {
	var resp any
	var err error

	if r == nil {
		return resp, fmt.Errorf("%w: response writer is nil", ErrInvalidResponse)
	}

	switch len(r.values) {
	case 0:
		return resp, nil
	case 1:
		ret0 := r.values[0].Interface()
		if err, ok := ret0.(error); ok {
			return resp, err
		}
	case 2:
		ret1 := r.values[1].Interface()
		if ret1 != nil {
			if _, ok := ret1.(error); !ok {
				kind := r.values[1].Type().String()
				return resp, fmt.Errorf("%w: service response expected to contain an error - instead has %s", ErrInvalidResponse, kind)
			}
			err = ret1.(error)
		}
		resp = r.values[0].Interface()
	default:
		return resp, fmt.Errorf("%w: service response expected 0-2 results but received %d", ErrInvalidResponse, len(r.values))
	}

	return resp, err
}

func (r *ResponseWriter) Err() error {
	if r == nil {
		return fmt.Errorf("%w: response writer is nil", ErrInvalidResponse)
	}

	if len(r.values) == 0 {
		return nil
	}

	lastRet := r.values[len(r.values)-1].Interface()
	if err, ok := lastRet.(error); ok {
		return err
	}

	return nil
}

// Values is a simple getter to retrieve the reflected values returned by
// the service implementation.  Interceptors may inspect these values and
// potentially modify them or trigger error handling.
func (r *ResponseWriter) Values() []reflect.Value {
	return r.values
}

// NextInterceptor is a function closure which holds the context and state to be invoked
// by Request.Next().
type NextInterceptor func() error

// Interceptor is a function that intercepts a service call.
type Interceptor func(r *Request, w *ResponseWriter) error

// Request is a struct that contains information about a service call.
type Request struct {
	ChainName string            // ChainName is the name of the chain that the service call is being intercepted by.
	Method    string            // Method is the name of the service method being called.
	Receiver  string            // Receiver is the name of the service implementation.
	Package   string            // Package is the package of the called service method.
	Caller    string            // Caller is the name of the function that called the service method.
	Args      []any             // Args is a slice of arguments passed to the service method excluding the context if provided.
	ctx       context.Context   // ctx is the context of the service call.
	next      NextInterceptor   // next is a function closure with context, state and the next interceptor in the chain.
	tags      map[string]string // list of metadata tags associated with this request.
}

// Tags returns the list of metadata tags associated with this request.
func (r *Request) Tags() map[string]string {
	return r.tags
}

// AddTags adds metadata tags to this request.
func (r *Request) AddTags(tags ...Tag) {
	for _, tag := range tags {
		r.tags[tag.Key] = tag.Value
	}
}

// Context returns the context of the service call.
func (r *Request) Context() context.Context {
	if r != nil {
		return r.ctx
	}
	return context.Background()
}

// SetContext sets the context of the service call.
func (r *Request) SetContext(ctx context.Context) *Request {
	if r != nil {
		r.ctx = ctx
	}
	return r
}

// Next is used by the interceptor implementation to invoke the next step in the chain.
// Interceptors can modify the return values and affect the final result.  It
// can also be invoked more than once to implement retry logic.
func (r *Request) Next() error {
	return r.next()
}

// Interceptors is a slice of interceptors which can be appended to and passed to
// NewChain to create a chain of interceptors.
type Interceptors []Interceptor

func (i Interceptors) Append(interceptors ...Interceptor) Interceptors {
	return append(i, interceptors...)
}

// Chain is a struct that contains a named chain of interceptors that can be bound to
// a service implementation.
type Chain struct {
	name         string
	interceptors Interceptors
	tags         []Tag
}

// ChainState is a struct that contains the state of a chain.
type ChainState struct {
	addContext bool
	f          any
	position   int
	request    *Request
}

// NewChain creates a named chain of interceptors from a slice.  The name can
// be used to identify the chain for logging or indicate the behaviors of a
// particular chain.
func NewChain(name string, interceptors Interceptors, tags ...Tag) *Chain {
	return &Chain{name: name, interceptors: interceptors, tags: tags}
}

// Duplicate is a convenience function to create a new chain with the same
// interceptors and tags as the original but with a different chain name.
func (s *Chain) Duplicate(name string) *Chain {
	return NewChain(name, s.interceptors, s.tags...)
}

func (s *Chain) getFunName(temp interface{}) string {
	if reflect.TypeOf(temp).Kind() != reflect.Func {
		return ""
	}
	funName := runtime.FuncForPC(reflect.ValueOf(temp).Pointer()).Name()
	funName = funName[strings.LastIndex(funName, "/")+1:]
	// Check if funName is a "synthetic name" - i.e. ends in "-fm" end
	// remove the "-fm" suffix.
	if strings.HasSuffix(funName, "-fm") {
		funName = funName[:len(funName)-3]
	}
	return funName
}

func (s *Chain) getFunNameFromFrames(skipFrames int) string {
	pc, _, _, ok := runtime.Caller(skipFrames)
	funName := ""
	if ok {
		funName = runtime.FuncForPC(pc).Name()
		funName = funName[strings.LastIndex(funName, "/")+1:]

	}
	return funName
}

// handle starts the chain (if any) and sets up the state which tracks the position
// within the chain and meta information about the invocation.
func (s *Chain) handle(ctx context.Context, callerOffset int, addContext bool, f any, args ...any) (*ResponseWriter, error) {
	writer := &ResponseWriter{}
	if s == nil {
		s = &Chain{}
	}
	fullMethod := s.getFunName(f)
	callerName := s.getFunNameFromFrames(3 + callerOffset)
	state := &ChainState{
		addContext: addContext,
		f:          f,
		position:   0,
		request: &Request{
			ChainName: s.name,
			Caller:    callerName,
			Args:      args,
			ctx:       ctx,
			tags:      map[string]string{},
		},
	}
	tokens := strings.Split(fullMethod, ".")
	if len(tokens) == 3 {
		state.request.Package = tokens[0]
		state.request.Receiver = tokens[1]
		state.request.Method = tokens[2]
	} else {
		state.request.Method = fullMethod
	}
	state.request.AddTags(s.tags...)

	err := s.next(ctx, writer, state)
	if err != nil {
		return nil, err
	}
	return writer, nil
}

// next either calls the next interceptor in the chain or terminates the chain
// by calling the service handler.
func (s *Chain) next(ctx context.Context, writer *ResponseWriter, state *ChainState) error {
	var values []reflect.Value = []reflect.Value{}

	if state.addContext {
		values = append(values, reflect.ValueOf(ctx))
	}

	thereAreRemaining := state.position < len(s.interceptors)
	if thereAreRemaining {
		state.position += 1
		state.request.next = func() error {
			return s.next(state.request.Context(), writer, state)
		}
		return s.interceptors[state.position-1](state.request, writer)
	}

	// No more interceptors, call the service handler.
	values = append(values, lo.Map(state.request.Args, func(arg any, index int) reflect.Value { return reflect.ValueOf(arg) })...)

	writer.SetValues(reflect.ValueOf(state.f).Call(values)...)

	return writer.Err()
}

// HandlerImplementationNoArg is a type alias which defines the signature that a service implementation
// function must implement that no arg and a result.
type HandlerImplementation0[R any] interface {
	func(context.Context) (R, error)
}

// HandlerImplementation is a type alias which defines the signature that a service implementation
// function must implement that has an arg and a result.
type HandlerImplementation1[A, R any] interface {
	func(context.Context, A) (R, error)
}

// HandlerImplementation2 is a type alias which defines the signature that a service implementation
// function must implement that has two args and a result.
type HandlerImplementation2[A1, A2, R any] interface {
	func(context.Context, A1, A2) (R, error)
}

// HandlerImplementationNoArgNoResult is a type alias which defines the signature that a service implementation
// function has no arg and no result.
type HandlerImplementationNoResult0 interface {
	func(context.Context) error
}

// HandlerImplementationNoResult is a type alias which defines the signature that a service implementation
// function must implement that has an arg and no result.
type HandlerImplementationNoResult1[A any] interface {
	func(context.Context, A) error
}

// HandlerImplementationNoResult2 is a type alias which defines the signature that a service implementation
// function must implement that no arg and a result.
type HandlerImplementationNoResult2[A1, A2 any] interface {
	func(context.Context, A1, A2) error
}

func HandleN(s *Chain, callerOffset int, f any, args ...any) ([]reflect.Value, error) {
	var ok bool
	var ctx context.Context
	var addContext bool
	if len(args) > 0 {
		if ctx, ok = args[0].(context.Context); ok {
			args = args[1:]
			addContext = true
		}
	}
	if !ok {
		ctx = context.Background()
	}
	result, err := s.handle(ctx, callerOffset, addContext, f, args...)
	if err != nil || result == nil {
		return []reflect.Value{}, err
	}

	return result.Values(), nil
}

// Handle0 is called by the service definition to invoke the interceptor chain and ultimately the service implementation
// provided by f.  The result types of f must match R.
func Handle0[T HandlerImplementation0[R], R any](s *Chain, callerOffset int, f T, ctx context.Context) (R, error) {
	var zeroResult R
	writer, err := s.handle(ctx, callerOffset, true, f)
	if err != nil || writer == nil {
		return zeroResult, err
	}

	result, err := writer.Response()
	if err != nil {
		return zeroResult, err
	}
	return result.(R), nil
}

// Handle1 is called by the service definition to invoke the interceptor chain and ultimately the service implementation
// provided by f.  The arguments and result types of f must match A and R, respectively.
func Handle1[T HandlerImplementation1[A, R], A, R any](s *Chain, callerOffset int, f T, ctx context.Context, a A) (R, error) {
	var zeroResult R
	writer, err := s.handle(ctx, callerOffset, true, f, a)
	if err != nil || writer == nil {
		return zeroResult, err
	}

	result, err := writer.Response()
	if err != nil {
		return zeroResult, err
	}
	return result.(R), nil
}

// Handle2 is called by the service definition to invoke the interceptor chain and ultimately the service implementation
// provided by f.  The arguments and result types of f must match A1, A2, and R, respectively.
func Handle2[T HandlerImplementation2[A1, A2, R], A1, A2, R any](s *Chain, callerOffset int, f T, ctx context.Context, a1 A1, a2 A2) (R, error) {
	var zeroResult R
	writer, err := s.handle(ctx, callerOffset, true, f, a1, a2)
	if err != nil || writer == nil {
		return zeroResult, err
	}

	result, err := writer.Response()
	if err != nil {
		return zeroResult, err
	}
	return result.(R), nil
}

// HandleNoResult0 is called by the service definition to invoke the interceptor chain and ultimately the service implementation
// provided by f.
func HandleNoResult0[T HandlerImplementationNoResult0](s *Chain, callerOffset int, f T, ctx context.Context) error {
	_, err := s.handle(ctx, callerOffset, true, f)
	if err != nil {
		return err
	}

	return nil
}

// HandleNoResult1 is called by the service definition to invoke the interceptor chain and ultimately the service implementation
// provided by f.  The arguments must match A.
func HandleNoResult1[T HandlerImplementationNoResult1[A], A any](s *Chain, callerOffset int, f T, ctx context.Context, a A) error {
	_, err := s.handle(ctx, callerOffset, true, f, a)
	if err != nil {
		return err
	}

	return nil
}

// HandleNoResult2 is called by the service definition to invoke the interceptor chain and ultimately the service implementation
// provided by f.  The arguments must match A1 and A2.
func HandleNoResult2[T HandlerImplementationNoResult2[A1, A2], A1, A2 any](s *Chain, callerOffset int, f T, ctx context.Context, a1 A1, a2 A2) error {
	_, err := s.handle(ctx, callerOffset, true, f, a1, a2)
	if err != nil {
		return err
	}

	return nil
}
