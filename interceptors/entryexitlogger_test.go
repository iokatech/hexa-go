package interceptors

import (
	"context"
	"fmt"

	"gitlab.com/iokatech/hexa-go"
	"gitlab.com/iokatech/hexa-go/utils"
)

func ExampleEntryExitLogger() {
	testFun := func(ctx context.Context, arg hexa.EmptyArg) (string, error) {
		fmt.Println("in testfun")
		return "Hello", nil
	}

	logger := utils.NewStdOutLogger()

	interceptor := EntryExitLogger(logger, utils.LogLevelInfo)

	defaultInterceptors := hexa.Interceptors([]hexa.Interceptor{interceptor})

	chain := hexa.NewChain("test", defaultInterceptors)

	result, _ := hexa.Handle1(chain, 0, testFun, context.Background(), hexa.EmptyArg{})

	fmt.Println(result)

	// Output: (info): Entering test:interceptors.ExampleEntryExitLogger:func1
	// in testfun
	// (info): Exiting test:interceptors.ExampleEntryExitLogger:func1
	// Hello
}
