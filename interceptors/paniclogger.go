package interceptors

import (
	"fmt"
	"runtime/debug"

	"gitlab.com/iokatech/hexa-go"
	"gitlab.com/iokatech/hexa-go/utils"
)

// ErrPanic is the error type returned by the PanicLogger interceptor and wraps
// the original panic along with the stack trace.
type ErrPanic struct {
	err   error
	stack []byte
}

func (e ErrPanic) Error() string {
	return fmt.Sprintf("%v:\n%v", e.err, string(e.stack))
}

func (e ErrPanic) Unwrap() error {
	return e.err
}

func (e ErrPanic) Cause() error {
	return e.err
}

func NewErrPanic(err error) ErrPanic {
	return ErrPanic{
		err:   err,
		stack: debug.Stack(),
	}
}

// PanicLogger auto recovers from panics and logs them.
//
// `logger` is the logger to use. This can be any logger that implements the
// hexa-go/utils.Logger interface. You can use the NewDecoratedLoggerN functions
// to wrap your own logger.
func PanicLogger(logger utils.Logger) hexa.Interceptor {
	return func(r *hexa.Request, w *hexa.ResponseWriter) (err error) {
		defer func() {
			if rvr := recover(); rvr != nil {
				err = NewErrPanic(fmt.Errorf("%v", rvr))

				errPanic := err.(ErrPanic)

				logger.Errorf("panic: %v", errPanic.Cause().Error())
			}
		}()

		err = r.Next()
		return
	}
}
