package interceptors

import (
	"gitlab.com/iokatech/hexa-go"
	"gitlab.com/iokatech/hexa-go/utils"
)

// EntryExitLogger logs the entry and exit of a request.  It is useful for debugging.
//
// `logger` is the logger to use. This can be any logger that implements the
// hexa-go/utils.Logger interface. You can use the NewDecoratedLoggerN functions
// to wrap your own logger.
//
// `logLevel` is the log level to use.
func EntryExitLogger(logger utils.Logger, logLevel utils.LogLevel) hexa.Interceptor {
	return func(r *hexa.Request, w *hexa.ResponseWriter) error {
		logFun := logger.Debugf
		if logLevel == utils.LogLevelError {
			logFun = logger.Errorf
		} else if logLevel == utils.LogLevelInfo {
			logFun = logger.Infof
		} else if logLevel == utils.LogLevelWarn {
			logFun = logger.Warnf
		}

		logFun("Entering %s:%s:%s", r.ChainName, r.Caller, r.Method)

		defer func() {
			logFun("Exiting %s:%s:%s", r.ChainName, r.Caller, r.Method)
		}()

		return r.Next()
	}

}
