package interceptors

import (
	"context"
	"fmt"

	"gitlab.com/iokatech/hexa-go"
	"gitlab.com/iokatech/hexa-go/utils"
)

func ExamplePanicLogger() {
	testFun := func(ctx context.Context, arg int) (string, error) {
		panic("test panic")
	}

	logger := utils.NewStdOutLogger()

	interceptor := PanicLogger(logger)

	defaultInterceptors := hexa.Interceptors([]hexa.Interceptor{interceptor})

	chain := hexa.NewChain("test", defaultInterceptors)

	result, _ := hexa.Handle1(chain, 1, testFun, context.Background(), 5)

	fmt.Println(result)

	// Output: (error): panic: test panic
}
