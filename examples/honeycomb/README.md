### Honeycomb example

The `honeycomb` example can be run with

``` shell
go run ./examples/honeycomb/cmd
```

It demonstrates a prebuilt chain of interceptors that

* performs entry/exit logging,
* sets a value in the context,
* validates arguments passed into the service, and
* reads a value from the context.

The structure of the package emulates a real-world application using ports and adapters.
