package main

import (
	"gitlab.com/iokatech/hexa-go"
	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/adapters"
	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/adapters/postgres"
	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/interceptors"
	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/managers"
	honeybeemanager "gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/managers/honeybee"
	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/ports"
	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/utils"
	hexa_interceptors "gitlab.com/iokatech/hexa-go/interceptors"
	hexa_utils "gitlab.com/iokatech/hexa-go/utils"
)

type Factory struct {
	DefaultInterceptors []hexa.Interceptor
	RepositoryChain     *hexa.Chain
	ManagerChain        *hexa.Chain

	HoneyBeeRepository ports.HoneyBeeRepository
	HoneyBeeManager    managers.HoneyBeeManager
}

func Initialize() *Factory {
	logger := utils.HoneyCombLogger{}

	decorated_logger := hexa_utils.NewDecoratedLogger1(logger.Debugf, logger.Infof, logger.Warnf, logger.Errorf, "honey-prefix")

	defaultInterceptors := hexa.Interceptors([]hexa.Interceptor{
		hexa_interceptors.EntryExitLogger(decorated_logger, hexa_utils.LogLevelInfo),
		hexa_interceptors.PanicLogger(decorated_logger),
		interceptors.StatusAppender,
		interceptors.ArgValidator,
	})

	f := &Factory{
		// ManagerChain can use just the default interceptors
		ManagerChain: hexa.NewChain("manager_chain", defaultInterceptors),
		// RepositoryChain can use the default interceptors plus the adapter specific ones
		RepositoryChain: hexa.NewChain("adapter_chain", defaultInterceptors.Append(
			adapters.HoneyQualityValueSetter,
			adapters.HoneyQualityValueReader,
		)),
	}

	f.HoneyBeeRepository = ports.NewHoneyBeeRepositoryWithInterceptors(&postgres.Adapter{}, f.RepositoryChain)
	f.HoneyBeeManager = managers.NewHoneyBeeManagerWithInterceptors(honeybeemanager.New(f.HoneyBeeRepository), f.ManagerChain)

	return f
}
