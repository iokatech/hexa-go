package main

import (
	"context"
	"fmt"
)

func main() {
	ctx := context.Background()

	f := Initialize()

	bees, _, _ := f.HoneyBeeManager.ListHoneyBees(ctx, 0, 0)
	fmt.Println(bees)
}
