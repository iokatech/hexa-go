package main_test

import (
	"context"
	"testing"

	. "gitlab.com/iokatech/hexa-go/examples/honeycomb/cmd"
	honeybeemanager "gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/managers/honeybee"
	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/models"
	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/ports"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestHoneycomb(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "honeycomb Suite")
}

var _ = Describe("Service Interceptor tests", func() {
	var ctx context.Context
	var f *Factory

	BeforeEach(func() {
		f = Initialize()
		ctx = context.Background()
	})

	It("test no arg, no return", func() {
		err := f.HoneyBeeManager.DeleteHoneyBees(ctx)

		Expect(err).To(BeNil())
	})

	It("test with invalid arg, no return", func() {
		err := f.HoneyBeeManager.DeleteHoneyBee(ctx, ports.DeleteHoneyBee(""))

		Expect(err).ToNot(BeNil())
	})

	It("test with valid arg, no return", func() {
		err := f.HoneyBeeManager.DeleteHoneyBee(ctx, ports.DeleteHoneyBee("valid_id"))

		Expect(err).To(BeNil())
	})

	It("test with no arg, with return", func() {
		result, count, err := f.HoneyBeeManager.ListHoneyBees(ctx, 0, 0)

		Expect(err).To(BeNil())
		Expect(len(result)).To(Equal(1))
		Expect(count).To(Equal(1))
		Expect(result[0].Quality).To(Equal("super_tasty_honey"))
	})

	It("test with expecting an error", func() {
		_, err := f.HoneyBeeManager.GetHoneyBee(ctx, "notfound")

		Expect(err).ToNot(BeNil())
	})

	It("test with invalid arg, with return", func() {
		_, err := f.HoneyBeeManager.UpdateHoneyBee(ctx, ports.UpsertHoneyBee{
			Bee: models.HoneyBee{
				Age: 0,
			},
		})

		Expect(err).ToNot(BeNil())
	})

	It("test with valid arg, with return", func() {
		_, err := f.HoneyBeeManager.UpdateHoneyBee(ctx, ports.UpsertHoneyBee{
			Bee: models.HoneyBee{
				Age: 1,
			},
		})

		Expect(err).To(BeNil())
	})

	It("test with invalid arg, no context", func() {
		err := f.HoneyBeeManager.StartHive(honeybeemanager.StartHiveCommand{TotalBees: 0})

		Expect(err).ToNot(BeNil())
	})

	It("test with valid arg, no context", func() {
		err := f.HoneyBeeManager.StartHive(honeybeemanager.StartHiveCommand{TotalBees: 1})

		Expect(err).To(BeNil())
	})

	It("test with no arg, no context, no result", func() {
		f.HoneyBeeManager.StopHive()

		// just make sure no panics happen
	})

	It("test modifying return value in interceptor", func() {
		status := f.HoneyBeeManager.GetHiveStatus()

		Expect(status.GetStatus("hive-status")).To(Equal("good"))
		Expect(status.GetStatus("interceptor-status")).To(Equal("good"))
	})
})
