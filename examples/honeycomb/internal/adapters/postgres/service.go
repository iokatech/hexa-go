package postgres

import (
	"context"
	"fmt"

	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/adapters"
	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/models"
	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/ports"
)

type Result struct {
	Success bool
	Count   int
}

type Adapter struct{}

func (s *Adapter) ListHoneyBees(ctx context.Context, offset int, limit int) ([]models.HoneyBee, error) {
	fmt.Println("In list HoneyBees")

	quality, _ := ctx.Value(adapters.HoneyQualityCtxKey).(string)

	return []models.HoneyBee{{ID: "1", Queen: true, Quality: quality}}, nil
}

func (s *Adapter) GetHoneyBee(ctx context.Context, arg string) (models.HoneyBee, error) {
	fmt.Println("In get HoneyBee")

	quality, _ := ctx.Value(adapters.HoneyQualityCtxKey).(string)

	return models.HoneyBee{ID: "1", Queen: true, Quality: quality}, nil
}

func (s *Adapter) CreateHoneyBee(ctx context.Context, arg ports.UpsertHoneyBee) (models.HoneyBee, error) {
	fmt.Println("In create HoneyBee")

	quality, _ := ctx.Value(adapters.HoneyQualityCtxKey).(string)

	return models.HoneyBee{ID: "1", Queen: true, Quality: quality}, nil
}

func (s *Adapter) UpdateHoneyBee(ctx context.Context, arg ports.UpsertHoneyBee) (models.HoneyBee, error) {
	fmt.Println("In update HoneyBee")

	quality, _ := ctx.Value(adapters.HoneyQualityCtxKey).(string)

	return models.HoneyBee{ID: "1", Queen: true, Quality: quality}, nil
}

func (s *Adapter) DeleteHoneyBee(ctx context.Context, arg ports.DeleteHoneyBee) error {
	fmt.Println("In delete HoneyBee")

	return nil
}

func (s *Adapter) DeleteHoneyBees(ctx context.Context) error {
	fmt.Println("In delete HoneyBees")

	return nil
}
