package adapters

import (
	"context"
	"fmt"

	"gitlab.com/iokatech/hexa-go"
)

type contextKey string

var HoneyQualityCtxKey contextKey = "honey_quality_key"

func HoneyQualityValueReader(r *hexa.Request, w *hexa.ResponseWriter) error {
	fmt.Println("in context value reader")
	defer func() {
		fmt.Println("in defer context value reader")
		ctxValue := r.Context().Value(HoneyQualityCtxKey)
		if ctxValue != nil {
			fmt.Printf("test ctx value is %v\n", ctxValue)
		}
	}()
	return r.Next()
}

func HoneyQualityValueSetter(r *hexa.Request, w *hexa.ResponseWriter) error {
	fmt.Println("in context value setter")
	r.SetContext(context.WithValue(r.Context(), HoneyQualityCtxKey, "super_tasty_honey"))
	defer func() {
		fmt.Println("in defer context value setter")
	}()
	return r.Next()
}
