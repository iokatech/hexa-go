package models

type HoneyBee struct {
	ID      string `json:"id"`
	Queen   bool   `json:"queen"`
	Age     int    `json:"age"`
	Quality string `json:"quality"`
}
