package utils

import "fmt"

type HoneyCombLogger struct{}

// Debugf logs a debug message
func (r HoneyCombLogger) Debugf(prefix string, format string, args ...any) {
	fmt.Printf("(debug): %s: %s\n", prefix, fmt.Sprintf(format, args...))
}

// Infof logs an info message
func (r HoneyCombLogger) Infof(prefix string, format string, args ...any) {
	fmt.Printf("(info): %s: %s\n", prefix, fmt.Sprintf(format, args...))
}

// Warnf logs a warning message
func (r HoneyCombLogger) Warnf(prefix string, format string, args ...any) {
	fmt.Printf("(warn): %s: %s\n", prefix, fmt.Sprintf(format, args...))
}

// Errorf logs an error message
func (r HoneyCombLogger) Errorf(prefix string, format string, args ...any) {
	fmt.Printf("(error): %s: %s\n", prefix, fmt.Sprintf(format, args...))
}
