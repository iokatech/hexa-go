package interceptors

import (
	"fmt"

	"gitlab.com/iokatech/hexa-go"
)

// StatusSetter represents a value that can set a status
type StatusSetter interface {
	SetStatus(key, value string) error
}

func StatusAppender(r *hexa.Request, w *hexa.ResponseWriter) error {
	fmt.Println("in status appender")
	defer func() {
		if len(w.Values()) == 0 {
			return
		}
		firstValue := w.Values()[0].Interface()
		if firstValue == nil {
			return
		}
		if headerSetter, ok := firstValue.(StatusSetter); ok {
			headerSetter.SetStatus("interceptor-status", "good")
		}
	}()

	err := r.Next()
	if err != nil {
		fmt.Printf("got error in status appender setter: %v\n", err)
	}
	return err
}
