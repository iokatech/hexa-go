package interceptors

import (
	"fmt"

	"gitlab.com/iokatech/hexa-go"
)

// Validator provides an interface for argument validation
type Validator interface {
	Validate() error
}

func ArgValidator(r *hexa.Request, w *hexa.ResponseWriter) error {
	fmt.Println("in arg validator")
	defer func() {
		fmt.Println("in defer arg validator")
	}()
	if len(r.Args) > 0 {
		cmd, ok := r.Args[0].(Validator)
		if ok {
			err := cmd.Validate()
			if err != nil {
				return err
			}
		}
	}
	err := r.Next()
	if err != nil {
		fmt.Printf("got error in arg validator: %v\n", err)
	}
	return err
}
