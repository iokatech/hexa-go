//go:generate go run github.com/rjeczalik/interfaces/cmd/interfacer -for gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/managers/honeybee.Service -as managers.HoneyBeeManager -o ../honeybeemanager.go
//go:generate go run github.com/hexdigest/gowrap/cmd/gowrap gen -g -p gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/managers -i HoneyBeeManager -t ../../../../../templates/hexa -o ../honeybeemanager.factory.go

package honeybeemanager

import (
	"context"
	"fmt"

	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/models"
	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/ports"
)

type Service struct {
	r ports.HoneyBeeRepository
}

type StartHiveCommand struct {
	TotalBees int
}

func (c StartHiveCommand) Validate() error {
	if c.TotalBees < 1 {
		return fmt.Errorf("invalid total bees specified: %d", c.TotalBees)
	}
	return nil
}

type HiveStatus struct {
	status map[string]string
}

func (s *HiveStatus) SetStatus(key, value string) error {
	s.status[key] = value
	return nil
}

func (s *HiveStatus) GetStatus(key string) string {
	return s.status[key]
}

func New(r ports.HoneyBeeRepository) *Service {
	return &Service{
		r: r,
	}
}

func (m *Service) ListHoneyBees(ctx context.Context, offset int, limit int) ([]models.HoneyBee, int, error) {
	bees, err := m.r.ListHoneyBees(ctx, offset, limit)
	return bees, len(bees), err
}

func (m *Service) GetHoneyBee(ctx context.Context, id string) (models.HoneyBee, error) {
	if id == "notfound" {
		return models.HoneyBee{}, fmt.Errorf("not found")
	}
	return m.r.GetHoneyBee(ctx, id)
}

func (m *Service) CreateHoneyBee(ctx context.Context, upsertHoneyBee ports.UpsertHoneyBee) (models.HoneyBee, error) {
	return m.r.CreateHoneyBee(ctx, upsertHoneyBee)
}

func (m *Service) UpdateHoneyBee(ctx context.Context, upsertHoneyBee ports.UpsertHoneyBee) (models.HoneyBee, error) {
	return m.r.UpdateHoneyBee(ctx, upsertHoneyBee)
}

func (m *Service) DeleteHoneyBee(ctx context.Context, id ports.DeleteHoneyBee) error {
	return m.r.DeleteHoneyBee(ctx, id)
}

func (m *Service) DeleteHoneyBees(ctx context.Context) error {
	return m.r.DeleteHoneyBees(ctx)
}

func (m *Service) StartHive(cmd StartHiveCommand) error {
	return nil
}

func (m *Service) StopHive() {

}

func (m *Service) GetHiveStatus() *HiveStatus {
	status := HiveStatus{status: make(map[string]string)}
	status.SetStatus("hive-status", "good")
	return &status
}
