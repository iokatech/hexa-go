package ports

import (
	"fmt"

	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/models"
)

type UpsertHoneyBee struct {
	Bee models.HoneyBee
}

func (c UpsertHoneyBee) Validate() error {
	if c.Bee.Age <= 0 {
		return fmt.Errorf("invalid age for honeybee: %d", c.Bee.Age)
	}
	return nil
}

type DeleteHoneyBee string

func (c DeleteHoneyBee) Validate() error {
	if c == "" {
		return fmt.Errorf("value must be specified")
	}
	return nil
}
