//go:generate go run github.com/hexdigest/gowrap/cmd/gowrap gen -g -p gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/ports -i HoneyBeeRepository -t ../../../../templates/hexa -o honeybeerepository.factory.go

package ports

import (
	"context"

	"gitlab.com/iokatech/hexa-go/examples/honeycomb/internal/models"
)

type HoneyBeeRepository interface {
	ListHoneyBees(context.Context, int, int) ([]models.HoneyBee, error)
	GetHoneyBee(context.Context, string) (models.HoneyBee, error)
	CreateHoneyBee(context.Context, UpsertHoneyBee) (models.HoneyBee, error)
	UpdateHoneyBee(context.Context, UpsertHoneyBee) (models.HoneyBee, error)
	DeleteHoneyBee(context.Context, DeleteHoneyBee) error
	DeleteHoneyBees(context.Context) error
}
