//go:generate go run github.com/rjeczalik/interfaces/cmd/interfacer -for gitlab.com/iokatech/hexa-go/examples/minimal.MyService -as gen.MyService -o gen/myservice.go
//go:generate go run github.com/hexdigest/gowrap/cmd/gowrap gen -g -p gitlab.com/iokatech/hexa-go/examples/minimal/gen -i MyService -t ../../templates/hexa -o gen/myservice.factory.go

// Run example with `go run ./examples/minimal`
package main

import (
	"context"
	"fmt"

	"gitlab.com/iokatech/hexa-go"
	"gitlab.com/iokatech/hexa-go/examples/minimal/gen"
)

type MyService struct{}

// Service definition the caller imports and uses
func (s *MyService) ServiceMethod(ctx context.Context, arg string) (bool, error) {
	fmt.Printf("Received arg: %s\n", arg)
	return true, nil
}

// Interceptor definition
func MyInterceptor(request *hexa.Request, writer *hexa.ResponseWriter) error {
	// Do something in the defer path
	defer func() {
		fmt.Println("MyInterceptor end!")
	}()

	// Do something
	fmt.Println("MyInterceptor begin!")

	return request.Next()
}

func main() {
	// Create a chain of interceptors
	chain := hexa.NewChain("my_service", hexa.Interceptors([]hexa.Interceptor{MyInterceptor}))
	myService := gen.NewMyServiceWithInterceptors(&MyService{}, chain)

	// Call the service directly with no knowledge of the interceptor chain
	res, err := myService.ServiceMethod(context.Background(), "test")

	fmt.Printf("Result: %v, Error: %v\n", res, err)
}
