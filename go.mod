module gitlab.com/iokatech/hexa-go

go 1.20

require github.com/samber/lo v1.38.1

require golang.org/x/sys v0.7.0 // indirect

require (
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/onsi/ginkgo/v2 v2.3.1
	github.com/onsi/gomega v1.22.1
	golang.org/x/exp v0.0.0-20220303212507-bbda1eaf7a17 // indirect
	golang.org/x/net v0.9.0 // indirect
	golang.org/x/text v0.9.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
