package utils

import "fmt"

// Logger is an interface to provide minimal logging functionality used by the
// interceptors.
type Logger interface {
	Debugf(format string, args ...any)
	Infof(format string, args ...any)
	Warnf(format string, args ...any)
	Errorf(format string, args ...any)
}

// LogLevel is an enum for log levels
type LogLevel string

const (
	LogLevelDebug LogLevel = "debug"
	LogLevelInfo  LogLevel = "info"
	LogLevelWarn  LogLevel = "warn"
	LogLevelError LogLevel = "error"
)

type loggerFunctions[A1 any, A2 any, A3 any] struct {
	arg1 func(a1 A1, format string, args ...any)
	arg2 func(a1 A1, a2 A2, format string, args ...any)
	arg3 func(a1 A1, a2 A2, a3 A3, format string, args ...any)
}

// StdOutLogger is a logger that logs to stdout
type StdOutLogger struct{}

// Debugf logs a debug message
func (r StdOutLogger) Debugf(format string, args ...any) {
	fmt.Printf("(debug): %s\n", fmt.Sprintf(format, args...))
}

// Infof logs an info message
func (r StdOutLogger) Infof(format string, args ...any) {
	fmt.Printf("(info): %s\n", fmt.Sprintf(format, args...))
}

// Warnf logs a warning message
func (r StdOutLogger) Warnf(format string, args ...any) {
	fmt.Printf("(warn): %s\n", fmt.Sprintf(format, args...))
}

// Errorf logs an error message
func (r StdOutLogger) Errorf(format string, args ...any) {
	fmt.Printf("(error): %s\n", fmt.Sprintf(format, args...))
}

// NewStdOutLogger creates a new StdOutLogger
func NewStdOutLogger() StdOutLogger {
	return StdOutLogger{}
}

// DecoratedLogger implements the minimal logging functionality defined by
// the Logger interface.  It allows the user to use their own logger that may
// have one to three arguments in the logging functions.  It wraps the user's
// logger and implements just the Logger interface.
type DecoratedLogger[A1 any, A2 any, A3 any] struct {
	a1        A1
	a2        A2
	a3        A3
	debugFuns loggerFunctions[A1, A2, A3]
	infoFuns  loggerFunctions[A1, A2, A3]
	warnFuns  loggerFunctions[A1, A2, A3]
	errorFuns loggerFunctions[A1, A2, A3]
}

func (r DecoratedLogger[A1, A2, A3]) log(funs *loggerFunctions[A1, A2, A3], format string, args ...any) {
	if funs.arg1 != nil {
		funs.arg1(r.a1, format, args...)
		return
	}
	if funs.arg2 != nil {
		funs.arg2(r.a1, r.a2, format, args...)
		return
	}
	if funs.arg3 != nil {
		funs.arg3(r.a1, r.a2, r.a3, format, args...)
		return
	}
}

// Debugf logs at debug level
func (r DecoratedLogger[A1, A2, A3]) Debugf(format string, args ...any) {
	r.log(&r.debugFuns, format, args...)
}

// Infof logs at info level
func (r DecoratedLogger[A1, A2, A3]) Infof(format string, args ...any) {
	r.log(&r.infoFuns, format, args...)
}

// Warnf logs at warn level
func (r DecoratedLogger[A1, A2, A3]) Warnf(format string, args ...any) {
	r.log(&r.warnFuns, format, args...)
}

// Errorf logs at error level
func (r DecoratedLogger[A1, A2, A3]) Errorf(format string, args ...any) {
	r.log(&r.errorFuns, format, args...)
}

// NewDecoratedLogger1 creates a new DecoratedLogger that wraps the user's logger.
//
// The user's logger must have one argument extra argument at the
// start of each logging function.  The first argument represents an arbitrary
// argument that is required by the wrapped logger.  The remaining arguments
// are the same as the standard logger functions.
func NewDecoratedLogger1[A1 any](debug func(a1 A1, format string, args ...any),
	infoFun func(a1 A1, format string, args ...any),
	warnFun func(a1 A1, format string, args ...any),
	errorFun func(a1 A1, format string, args ...any), a1 A1) DecoratedLogger[A1, any, any] {
	return DecoratedLogger[A1, any, any]{
		a1: a1,
		debugFuns: loggerFunctions[A1, any, any]{
			arg1: debug,
		},
		infoFuns: loggerFunctions[A1, any, any]{
			arg1: infoFun,
		},
		warnFuns: loggerFunctions[A1, any, any]{
			arg1: warnFun,
		},
		errorFuns: loggerFunctions[A1, any, any]{
			arg1: errorFun,
		},
	}
}

// NewDecoratedLogger2 creates a new DecoratedLogger that wraps the user's logger.
//
// The user's logger must have two extra arguments at the start of each logging
// function.  The first two arguments represent arbitrary arguments that are
// required by the wrapped logger.  The remaining arguments are the same as the
// standard logger functions.
func NewDecoratedLogger2[A1 any, A2 any](debug func(a1 A1, a2 A2, format string, args ...any),
	infoFun func(a1 A1, a2 A2, format string, args ...any),
	warnFun func(a1 A1, a2 A2, format string, args ...any),
	errorFun func(a1 A1, a2 A2, format string, args ...any), a1 A1, a2 A2) DecoratedLogger[A1, A2, any] {
	return DecoratedLogger[A1, A2, any]{
		a1: a1,
		a2: a2,
		debugFuns: loggerFunctions[A1, A2, any]{
			arg2: debug,
		},
		infoFuns: loggerFunctions[A1, A2, any]{
			arg2: infoFun,
		},
		warnFuns: loggerFunctions[A1, A2, any]{
			arg2: warnFun,
		},
		errorFuns: loggerFunctions[A1, A2, any]{
			arg2: errorFun,
		},
	}
}

// NewDecoratedLogger3 creates a new DecoratedLogger that wraps the user's logger.
//
// The user's logger must have three extra arguments at the start of each logging
// function.  The first three arguments represent arbitrary arguments that are
// required by the wrapped logger.  The remaining arguments are the same as the
// standard logger functions.
func NewDecoratedLogger3[A1 any, A2 any, A3 any](debug func(a1 A1, a2 A2, a3 A3, format string, args ...any),
	infoFun func(a1 A1, a2 A2, a3 A3, format string, args ...any),
	warnFun func(a1 A1, a2 A2, a3 A3, format string, args ...any),
	errorFun func(a1 A1, a2 A2, a3 A3, format string, args ...any), a1 A1, a2 A2, a3 A3) DecoratedLogger[A1, A2, A3] {
	return DecoratedLogger[A1, A2, A3]{
		a1: a1,
		a2: a2,
		a3: a3,
		debugFuns: loggerFunctions[A1, A2, A3]{
			arg3: debug,
		},
		infoFuns: loggerFunctions[A1, A2, A3]{
			arg3: infoFun,
		},
		warnFuns: loggerFunctions[A1, A2, A3]{
			arg3: warnFun,
		},
		errorFuns: loggerFunctions[A1, A2, A3]{
			arg3: errorFun,
		},
	}
}
