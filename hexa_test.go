package hexa

import (
	"context"
	"fmt"
	"testing"

	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
)

func TestHexa(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "hexa Suite")
}

type TestParams struct {
	Method string
	Caller string
}

// Handler is a demonstration of the handler layer that always returns
// true if it is called.  The interceptor provides validation and is
// responsible for the false cases.
func Handler(ctx context.Context, params TestParams) (bool, error) {
	return true, nil
}

// Service is a demonstration of the service layer that validates the test
// parameters match what the test passes in.
func Service(ctx context.Context, params TestParams) (bool, error) {
	s := &Chain{
		name: "handle_validations",
		interceptors: []Interceptor{
			// Return an error if the test parameters don't match the request.
			func(req *Request, writer *ResponseWriter) error {
				expectedMethod := req.Args[0].(TestParams).Method
				if req.Method != expectedMethod {
					return fmt.Errorf("expected method to be %s, got %s", expectedMethod, req.Method)
				}
				expectedCaller := req.Args[0].(TestParams).Caller
				if req.Caller != expectedCaller {
					return fmt.Errorf("expected caller to be %s, got %s", expectedCaller, req.Caller)
				}

				return req.Next()
			},
		},
	}

	return Handle1(s, 1, Handler, ctx, params)
}

// Caller wraps the service to simulate another package calling into the service.
func Caller(p TestParams) (bool, error) {
	return Service(context.Background(), p)
}

var _ = Describe("Service Interceptor tests", func() {

	It("should validate the request caller and method called", func() {

		got, err := Caller(TestParams{Method: "hexa-go.Handler", Caller: "hexa-go.Caller"})
		Expect(err).To(BeNil())
		Expect(got).To(Equal(true))
	})
})
