# Hexa Interceptor Framework

The `hexa-go` package provides seamless interceptor integration within Hexagonal
Architecture apps.

## Installation

``` bash
go get -u gitlab.com/iokatech/hexa-go
```

## Overview

`hexa-go` is an open-source Go package designed to streamline the integration of
interceptors within applications using a Hexagonal or Ports and Adapters architecture.
With `hexa-go`, the service layer of a Hexagonal component is connected to a middleware
chain which can be used to instrument service calls and interact with the context,
request and result using DRY code.

Using `hexa-go`, applications that decouple components with a service layer
can import or encapsulate a range of different interceptors to augment the behavior
when invoking a service method.

For example the following architectures work well:

* [Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)
* [Hexagonal Architecture](https://alistair.cockburn.us/hexagonal-architecture/)
* [Ports and Adapters](https://herbertograca.com/2017/09/14/ports-adapters-architecture/)
* [Onion Architecture](https://jeffreypalermo.com/2008/07/the-onion-architecture-part-1/)
* [Domain Driven Design](https://en.wikipedia.org/wiki/Domain-driven_design)

The behaviors and side-effects are useful for a range of different use cases, especially
instrumentation for Application Performance Measurement. Also:

* logging entry/exit points
* uniform error handling
* recovering from panics
* latency measurement

This framework empowers developers to build a robust service ecosystem, where each service
can be equipped with domain specific interceptors, pre-configured global interceptor chains
or third-party interceptors with no modification to service consumers using strong-typing.
The implementation requires minimal boiler plate to inject the chain.

## Usage

Define interceptors to provide the desired functionality and in the service definition
layer invoke the interceptor chain.  Clients still see the original interface and
implementations only need to adhere to the `ServiceImplementation` type.

A detailed example, [honeycomb](examples/honeycomb/README.md), demonstrates usage and
application organization.

The [minimum implementation](examples/minimal/main.go) looks like:

``` go
//go:generate go run github.com/rjeczalik/interfaces/cmd/interfacer -for gitlab.com/iokatech/hexa-go/examples/minimal.MyService -as gen.MyService -o gen/myservice.go
//go:generate go run github.com/hexdigest/gowrap/cmd/gowrap gen -g -p gitlab.com/iokatech/hexa-go/examples/minimal/gen -i MyService -t ../../templates/hexa -o gen/myservice.factory.go

// Run example with `go run ./examples/minimal`
package main

import (
	"context"
	"fmt"

	"gitlab.com/iokatech/hexa-go"
	"gitlab.com/iokatech/hexa-go/examples/minimal/gen"
)

type MyService struct{}

// Service definition the caller imports and uses
func (s *MyService) ServiceMethod(ctx context.Context, arg string) (bool, error) {
	fmt.Printf("Received arg: %s\n", arg)
	return true, nil
}

// Interceptor definition
func MyInterceptor(request *hexa.Request, writer *hexa.ResponseWriter) error {
	// Do something in the defer path
	defer func() {
		fmt.Println("MyInterceptor end!")
	}()

	// Do something
	fmt.Println("MyInterceptor begin!")

	return request.Next()
}

func main() {
	// Create a chain of interceptors
	chain := hexa.NewChain("my_service", hexa.Interceptors([]hexa.Interceptor{MyInterceptor}))
	myService := gen.NewMyServiceWithInterceptors(&MyService{}, chain)

	// Call the service directly with no knowledge of the interceptor chain
	res, err := myService.ServiceMethod(context.Background(), "test")

	fmt.Printf("Result: %v, Error: %v\n", res, err)
}

```

### Service Definition Generation

It is not a requirement, but to facilitate writing the service definition layer
you can use the code generation tool from [gowrap](https://github.com/hexdigest/gowrap/cmd/gowrap)
and from [interfacer](https://github.com/rjeczalik/interfaces/cmd/interfacer).
See the [honeybeemanager.go](examples/honeycomb/internal/managers/honeybee/honeybeemanager.go).
There are `//go generate` lines at the top that use `interfacer` and `gowrap` to
automatically create the interface definition for the manager
`honeybeemanager.go` and the corresponding factory `honeybeemanager.factory.go`.
The factory has a `New` function to create the service definition that simply
takes the original service implementation object and a user defined Hexa chain.

There is a template under `templates/hexa` that can be used with `gowrap` to
auto generate the service definition.

## Interceptors

`hexa-go` also provides prebuilt interceptors that can be imported:

* [Entry and exit logging](interceptors/entryexitlogger.go)
* [Panic logging and recovery](interceptors/paniclogger.go)
